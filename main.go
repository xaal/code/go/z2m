package main

import (
	"log/slog"

	"gitlab.imt-atlantique.fr/xaal/code/go/core/xaal"
)

func main() {
	cfg, err := parseConfig()
	if err != nil {
		slog.Error("Error parsing configuration file: ", "err", err)
		return
	}

	// start xAAL stack
	xaal.SetupLogger(cfg.logLevel)
	eng := xaal.NewEngine()
	// start the gateway
	gw := NewGW(cfg, eng)
	// start the xAAL engine
	eng.Run()
	// Engine stops, disconnect MQTT
	gw.Client.Disconnect(250)
	slog.Debug("MQTT disconnected")
}

package main

import (
	"fmt"
	"log/slog"

	"gitlab.imt-atlantique.fr/xaal/code/go/core/uuid"
	"gitlab.imt-atlantique.fr/xaal/code/go/core/xaal"
	"gopkg.in/ini.v1"
)

var (
	mqttClientID  = "z2m-" + uuid.New().String()
	ignoredTopics = []string{
		"bridge/groups",
		"bridge/definitions",
		"bridge/extensions",
		"bridge/info",
		"bridge/state",
		"bridge/logging",
		"bridge/config",
		"bridge/event",
		"bridge/log",
	}
)

type Config struct {
	brokerHost    string
	topic         string
	ignoredTopics []string
	logLevel      slog.Level
	brokerPort    int
	baseAddr      uuid.UUID
}

func parseConfig() (*Config, error) {
	// Default configuration
	config := Config{brokerHost: "mqtt", topic: "zigbee2mqtt", logLevel: slog.LevelError, brokerPort: 1883}
	// Parse the configuration file
	cfg, err := ini.Load(xaal.GetConfigDir() + "/z2m.ini")
	if err != nil {
		return nil, err
	}
	sec := cfg.Section(ini.DefaultSection)

	// NOTE: This parsing is painful to write, I should look for a better way to do it

	// broker
	broker, err := sec.GetKey("broker")
	if err == nil {
		config.brokerHost = broker.String()
	}
	// topic
	topic, err := sec.GetKey("topic")
	if err == nil {
		config.topic = topic.String()
	}
	// log level
	level, err := sec.GetKey("log_level")
	if err == nil {
		switch level.String() {
		case "DEBUG":
			config.logLevel = slog.LevelDebug
		case "INFO":
			config.logLevel = slog.LevelInfo
		case "WARN":
			config.logLevel = slog.LevelWarn
		default:
			config.logLevel = slog.LevelError
		}
	}
	// port
	port, err := sec.GetKey("port")
	if err == nil {
		value, err := port.Int()
		if err != nil {
			return nil, err
		}
		config.brokerPort = value
	}
	// base address
	addr, err := sec.GetKey("base_addr")
	if err == nil {
		baseAddr, err := uuid.FromString(addr.String())
		if err != nil {
			return nil, err
		}
		config.baseAddr = baseAddr
	}
	for _, topic := range ignoredTopics {
		config.ignoredTopics = append(config.ignoredTopics, fmt.Sprintf("%s/%s", config.topic, topic))
	}
	return &config, nil
}

# xAAL Gateway for Z2M

The main goal of this project is to provide a xAAL Gateway for Zigbee devices.
After severals tests with [ZHA](https://www.home-assistant.io/integrations/zha/),
we decided to switch to [Zigbee2MQTT](https://www.zigbee2mqtt.io/).

The main reasons are:

- ZHA provides a great API, but a lot of devices are not supported.
- Z2M supports a lot of devices and provides a nice web interface to fine tune devices.
- Z2M is written in NodeJS and we don't support JS right now.
- Z2M is a standalone application, so we can run it in a container.

This gateway simply forwards Z2M events to xAAL bus and vice versa through MQTT.
You can now use Z2M without messing with MQTT. Simply run this gateway, Z2M and
a broker in a docker container. And you can use your Zigbee devices with xAAL.

## Installation

The easier (and secure) way to use the gateway is to run it in a docker container.
A docker-compose file is provided to run all services in
[the gitlab repository](https://gitlab.imt-atlantique.fr/xaal/code/go/docker).

## FAQ

- Why are you using this gateway instead of Z2M MQTT throught HomeAssistant ?
xAAL supports HomeAssistant out of the box. And I don't want to really on
MQTT (spof, bad security).
- Why are you using Go instead of NodeJS or Python ?
We don't have a native JS implementation of the xAAL stack right now. And I
mainly want to test the Go implementation.

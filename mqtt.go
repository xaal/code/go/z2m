package main

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

// MQTTSetup creates a new MQTT client
func MQTTSetup(cfg *Config, publishHandler mqtt.MessageHandler) mqtt.Client {
	// This JS style of creating a client is awfully verbose
	opts := mqtt.NewClientOptions().
		AddBroker(fmt.Sprintf("tcp://%s:%d", cfg.brokerHost, cfg.brokerPort)).
		SetClientID(mqttClientID).
		SetDefaultPublishHandler(publishHandler).
		SetConnectionLostHandler(MQTTLostCnx).
		SetMaxReconnectInterval(time.Minute * 2)

	opts = opts.SetOnConnectHandler(func(client mqtt.Client) {
		MQTTSubscribe(client, cfg)
	})

	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		// panic(token.Error())
		slog.Warn("MQTTSetup failed", "err", token.Error())
	}
	return client
}

func MQTTSubscribe(client mqtt.Client, cfg *Config) {
	topic := cfg.topic + "/#"
	token := client.Subscribe(topic, 0, nil)
	if token.Wait(); token.Error() != nil {
		slog.Warn("MQTTSubscribe failed", "topic", topic, "err", token.Error())
	}
	slog.Debug("Subscribed to", "Topic", topic)
}

func MQTTLostCnx(client mqtt.Client, err error) {
	slog.Warn("MQTT connection lost")
}

// MQTTDumpMsg displays the MQTT message
func MQTTDumpMsg(msg mqtt.Message) {
	var data map[string]interface{}
	err := json.Unmarshal(msg.Payload(), &data)
	if err != nil {
		slog.Error("Error decoding JSON %v", "topic", msg.Topic(), "err", err)
	}
	jsonDump("MQTT update", data)
}

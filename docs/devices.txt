

Smart Plug
╭───────────────────────────────╮
│ Def:0x70b3d52b600c1f74        │
├──────────┬────────────────────┤
│ IeeeAddr │ 0x70b3d52b600c1f74 │
│ Vendor   │ Tuya               │
│ Model    │ TS011F_plug_3      │
│ Type     │ Router             │
╰──────────┴────────────────────╯
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│ Exp:0x70b3d52b600c1f74                                                                           │
├─────────────────────┬─────────┬─────┬──────┬─────────┬───────────────────────────────────────────┤
│ NAME                │ TYPE    │ ACC │ UNIT │ VALUES  │ FEATURES: NAME[TYPE]-ACC-(UNIT){PROPERTY} │
├─────────────────────┼─────────┼─────┼──────┼─────────┼───────────────────────────────────────────┤
│                     │ switch  │  -- │      │         │ - state[binary]-RWN-(){state}             │
│ power_outage_memory │ enum    │ RWN │      │ on      │                                           │
│                     │         │     │      │ off     │                                           │
│                     │         │     │      │ restore │                                           │
│ indicator_mode      │ enum    │ RWN │      │ off     │                                           │
│                     │         │     │      │ off/on  │                                           │
│                     │         │     │      │ on/off  │                                           │
│                     │         │     │      │ on      │                                           │
│ power               │ numeric │   R │ W    │         │                                           │
│ current             │ numeric │   R │ A    │         │                                           │
│ voltage             │ numeric │   R │ V    │         │                                           │
│ energy              │ numeric │   R │ kWh  │         │                                           │
│                     │ lock    │  -- │      │         │ - state[binary]-RW-(){child_lock}         │
│ linkquality         │ numeric │   R │ lqi  │         │                                           │
╰─────────────────────┴─────────┴─────┴──────┴─────────┴───────────────────────────────────────────╯
╭───────────────────────────────╮
│ MQTT update                   │
├─────────────────────┬─────────┤
│ child_lock          │ UNLOCK  │
│ current             │ 0.05    │
│ energy              │ 0       │
│ indicator_mode      │ off/on  │
│ linkquality         │ 196     │
│ power               │ 4       │
│ power_outage_memory │ restore │
│ state               │ ON      │
│ voltage             │ 241     │
╰─────────────────────┴─────────╯


Led strip Lidl
╭───────────────────────────────╮
│ Def:0x60a423fffe31c11b        │
├──────────┬────────────────────┤
│ IeeeAddr │ 0x60a423fffe31c11b │
│ Vendor   │ Lidl               │
│ Model    │ HG06104A           │
│ Type     │ Router             │
╰──────────┴────────────────────╯
╭─────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ Exp:0x60a423fffe31c11b                                                                                          │
├─────────────────────────┬─────────┬─────┬──────┬────────────────┬───────────────────────────────────────────────┤
│ NAME                    │ TYPE    │ ACC │ UNIT │ VALUES         │ FEATURES: NAME[TYPE]-ACC-(UNIT){PROPERTY}     │
├─────────────────────────┼─────────┼─────┼──────┼────────────────┼───────────────────────────────────────────────┤
│                         │ light   │  -- │      │                │ - state[binary]-RWN-(){state}                 │
│                         │         │     │      │                │ - brightness[numeric]-RWN-(){brightness}      │
│                         │         │     │      │                │ - color_temp[numeric]-RWN-(mired){color_temp} │
│                         │         │     │      │                │ - color_xy[composite]-RWN-(){color}           │
│ effect                  │ enum    │   W │      │ blink          │                                               │
│                         │         │     │      │ breathe        │                                               │
│                         │         │     │      │ okay           │                                               │
│                         │         │     │      │ channel_change │                                               │
│                         │         │     │      │ finish_effect  │                                               │
│                         │         │     │      │ stop_effect    │                                               │
│                         │         │     │      │ colorloop      │                                               │
│                         │         │     │      │ stop_colorloop │                                               │
│ do_not_disturb          │ binary  │  RW │      │                │                                               │
│ color_power_on_behavior │ enum    │  RW │      │ initial        │                                               │
│                         │         │     │      │ previous       │                                               │
│                         │         │     │      │ customized     │                                               │
│ linkquality             │ numeric │   R │ lqi  │                │                                               │
╰─────────────────────────┴─────────┴─────┴──────┴────────────────┴───────────────────────────────────────────────╯
╭──────────────────────────────────────────────────╮
│ MQTT update                                      │
├─────────────────────────┬────────────────────────┤
│ brightness              │ 126                    │
│ color                   │ map[x:0.2606 y:0.2083] │
│ color_mode              │ xy                     │
│ color_power_on_behavior │ previous               │
│ color_temp              │ 13                     │
│ do_not_disturb          │ true                   │
│ linkquality             │ 180                    │
│ state                   │ ON                     │
╰─────────────────────────┴────────────────────────╯

Led bulb Lidl
╭───────────────────────────────────────╮
│ Def:0x84fd27fffe92b162                │
├──────────┬────────────────────────────┤
│ IeeeAddr │ 0x84fd27fffe92b162         │
│ Vendor   │ Lidl                       │
│ Model    │ HG06492C/HG08130C/HG09154C │
│ Type     │ Router                     │
╰──────────┴────────────────────────────╯
╭────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ Exp:0x84fd27fffe92b162                                                                                 │
├────────────────┬─────────┬─────┬──────┬────────────────┬───────────────────────────────────────────────┤
│ NAME           │ TYPE    │ ACC │ UNIT │ VALUES         │ FEATURES: NAME[TYPE]-ACC-(UNIT){PROPERTY}     │
├────────────────┼─────────┼─────┼──────┼────────────────┼───────────────────────────────────────────────┤
│                │ light   │  -- │      │                │ - state[binary]-RWN-(){state}                 │
│                │         │     │      │                │ - brightness[numeric]-RWN-(){brightness}      │
│                │         │     │      │                │ - color_temp[numeric]-RWN-(mired){color_temp} │
│ effect         │ enum    │   W │      │ blink          │                                               │
│                │         │     │      │ breathe        │                                               │
│                │         │     │      │ okay           │                                               │
│                │         │     │      │ channel_change │                                               │
│                │         │     │      │ finish_effect  │                                               │
│                │         │     │      │ stop_effect    │                                               │
│ do_not_disturb │ binary  │  RW │      │                │                                               │
│ linkquality    │ numeric │   R │ lqi  │                │                                               │
╰────────────────┴─────────┴─────┴──────┴────────────────┴───────────────────────────────────────────────╯
╭─────────────────────────────╮
│ MQTT update                 │
├────────────────┬────────────┤
│ brightness     │ 254        │
│ color_mode     │ color_temp │
│ color_temp     │ 375        │
│ do_not_disturb │ false      │
│ linkquality    │ 200        │
│ state          │ ON         │
╰────────────────┴────────────╯

Power switch
╭───────────────────────────────╮
│ Def:0xa4c138ae7de0938d        │
├──────────┬────────────────────┤
│ IeeeAddr │ 0xa4c138ae7de0938d │
│ Vendor   │ MatSee Plus        │
│ Model    │ DAC2161C           │
│ Type     │ Router             │
╰──────────┴────────────────────╯
╭───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ Exp:0xa4c138ae7de0938d                                                                                                │
├────────────────────────┬─────────┬─────┬──────┬───────────────────────────┬───────────────────────────────────────────┤
│ NAME                   │ TYPE    │ ACC │ UNIT │ VALUES                    │ FEATURES: NAME[TYPE]-ACC-(UNIT){PROPERTY} │
├────────────────────────┼─────────┼─────┼──────┼───────────────────────────┼───────────────────────────────────────────┤
│                        │ switch  │  -- │      │                           │ - state[binary]-RW-(){state}              │
│ energy                 │ numeric │   R │ kWh  │                           │                                           │
│ power                  │ numeric │   R │ W    │                           │                                           │
│ voltage                │ numeric │   R │ V    │                           │                                           │
│ current                │ numeric │   R │ A    │                           │                                           │
│ fault                  │ enum    │   R │      │ clear                     │                                           │
│                        │         │     │      │ over_current_threshold    │                                           │
│                        │         │     │      │ over_power_threshold      │                                           │
│                        │         │     │      │ over_voltage threshold    │                                           │
│                        │         │     │      │ wrong_frequency_threshold │                                           │
│ threshold_1            │ enum    │   R │      │ not_set                   │                                           │
│                        │         │     │      │ over_current_threshold    │                                           │
│                        │         │     │      │ over_voltage_threshold    │                                           │
│ threshold_1_protection │ binary  │   R │      │                           │                                           │
│ threshold_1_value      │ numeric │   R │      │                           │                                           │
│ threshold_2            │ enum    │   R │      │ not_set                   │                                           │
│                        │         │     │      │ over_current_threshold    │                                           │
│                        │         │     │      │ over_voltage_threshold    │                                           │
│ threshold_2_protection │ binary  │   R │      │                           │                                           │
│ threshold_2_value      │ numeric │   R │      │                           │                                           │
│ clear_fault            │ binary  │  RW │      │                           │                                           │
│ meter_id               │ text    │   R │      │                           │                                           │
│ linkquality            │ numeric │   R │ lqi  │                           │                                           │
╰────────────────────────┴─────────┴─────┴──────┴───────────────────────────┴───────────────────────────────────────────╯

Power strip
╭───────────────────────────────╮
│ Def:0x60a423fffe802b9c        │
├──────────┬────────────────────┤
│ IeeeAddr │ 0x60a423fffe802b9c │
│ Vendor   │ UseeLink           │
│ Model    │ SM-O301-AZ         │
│ Type     │ Router             │
╰──────────┴────────────────────╯
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│ Exp:0x60a423fffe802b9c                                                                           │
├─────────────────────┬─────────┬─────┬──────┬─────────┬───────────────────────────────────────────┤
│ NAME                │ TYPE    │ ACC │ UNIT │ VALUES  │ FEATURES: NAME[TYPE]-ACC-(UNIT){PROPERTY} │
├─────────────────────┼─────────┼─────┼──────┼─────────┼───────────────────────────────────────────┤
│                     │ switch  │  -- │      │         │ - state[binary]-RWN-(){state_l1}          │
│                     │ switch  │  -- │      │         │ - state[binary]-RWN-(){state_l2}          │
│                     │ switch  │  -- │      │         │ - state[binary]-RWN-(){state_l3}          │
│                     │ switch  │  -- │      │         │ - state[binary]-RWN-(){state_l4}          │
│                     │ switch  │  -- │      │         │ - state[binary]-RWN-(){state_l5}          │
│ power_outage_memory │ enum    │ RWN │      │ on      │                                           │
│                     │         │     │      │ off     │                                           │
│                     │         │     │      │ restore │                                           │
│                     │ lock    │  -- │      │         │ - state[binary]-RW-(){child_lock}         │
│ linkquality         │ numeric │   R │ lqi  │         │                                           │
╰─────────────────────┴─────────┴─────┴──────┴─────────┴───────────────────────────────────────────╯
╭───────────────────╮
│ MQTT update       │
├─────────────┬─────┤
│ linkquality │ 255 │
│ state_l1    │ ON  │
│ state_l2    │ ON  │
│ state_l3    │ ON  │
│ state_l4    │ ON  │
│ state_l5    │ ON  │
╰─────────────┴─────╯

Ikea remote
╭───────────────────────────────╮
│ Def:0x287681fffe4a056e        │
├──────────┬────────────────────┤
│ IeeeAddr │ 0x287681fffe4a056e │
│ Vendor   │ IKEA               │
│ Model    │ E2213              │
│ Type     │ EndDevice          │
╰──────────┴────────────────────╯
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│ Exp:0x287681fffe4a056e                                                                           │
├─────────────┬─────────┬─────┬──────┬─────────────────┬───────────────────────────────────────────┤
│ NAME        │ TYPE    │ ACC │ UNIT │ VALUES          │ FEATURES: NAME[TYPE]-ACC-(UNIT){PROPERTY} │
├─────────────┼─────────┼─────┼──────┼─────────────────┼───────────────────────────────────────────┤
│ identify    │ enum    │   W │      │ identify        │                                           │
│ battery     │ numeric │  RN │ %    │                 │                                           │
│ action      │ enum    │   R │      │ 1_initial_press │                                           │
│             │         │     │      │ 1_long_press    │                                           │
│             │         │     │      │ 1_short_release │                                           │
│             │         │     │      │ 1_long_release  │                                           │
│             │         │     │      │ 1_double_press  │                                           │
│             │         │     │      │ 2_initial_press │                                           │
│             │         │     │      │ 2_long_press    │                                           │
│             │         │     │      │ 2_short_release │                                           │
│             │         │     │      │ 2_long_release  │                                           │
│             │         │     │      │ 2_double_press  │                                           │
│ linkquality │ numeric │   R │ lqi  │                 │                                           │
╰─────────────┴─────────┴─────┴──────┴─────────────────┴───────────────────────────────────────────╯

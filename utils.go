package main

import (
	"fmt"
	"math"
	"sort"
	"strconv"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/lucasb-eyer/go-colorful"
)

func hexStringToInteger(hexString string) (uint64, error) {
	// Drop the "0x" prefix if it exists
	if len(hexString) > 2 && hexString[:2] == "0x" {
		hexString = hexString[2:]
	}

	// Convert the hex string to an integer
	integerValue, err := strconv.ParseUint(hexString, 16, 64)
	if err != nil {
		return 0, err
	}
	return integerValue, nil
}

func jsonDump(title string, data map[string]interface{}) {
	// sort keys
	keys := make([]string, 0)
	for key := range data {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	// dump keys
	tab := table.NewWriter()
	tab.SetTitle(title)
	tab.SetStyle(table.StyleRounded)
	for _, key := range keys {
		if key != "update" {
			tab.AppendRow(table.Row{key, data[key]})
		}
	}
	fmt.Println(tab.Render())
}

// xyToColor converts CIE XY color coordinates and brightness to an RGB color using the go-colorful library.
// https://github.com/Koenkk/zigbee2mqtt/issues/3497
func xyToColor(x, y, brightness float64) colorful.Color {
	// Set maximum brightness if not provided (assuming brightness is in the range 0-254)
	if brightness == 0 {
		brightness = 254
	}

	// Calculate z, X, Y, and Z values for the XYZ color space
	z := 1.0 - x - y
	Y := brightness / 254.0 // Normalize brightness
	X := (Y / y) * x
	Z := (Y / y) * z

	// Convert XYZ to RGB using Wide RGB D65 conversion
	red := X*1.656492 - Y*0.354851 - Z*0.255038
	green := -X*0.707196 + Y*1.655397 + Z*0.036152
	blue := X*0.051713 - Y*0.121364 + Z*1.011530

	// If any color component is larger than 1.0, scale it back to 1.0 and adjust others accordingly
	if red > blue && red > green && red > 1.0 {
		green = green / red
		blue = blue / red
		red = 1.0
	} else if green > blue && green > red && green > 1.0 {
		red = red / green
		blue = blue / green
		green = 1.0
	} else if blue > red && blue > green && blue > 1.0 {
		red = red / blue
		green = green / blue
		blue = 1.0
	}

	// Apply reverse gamma correction to each component
	applyGamma := func(value float64) float64 {
		if value <= 0.0031308 {
			return 12.92 * value
		}
		return (1.0+0.055)*math.Pow(value, 1.0/2.4) - 0.055
	}

	red = applyGamma(red)
	green = applyGamma(green)
	blue = applyGamma(blue)
	return colorful.Color{R: red, G: green, B: blue}
}

func roundToDecimal(value float64, places int) float64 {
	pow := math.Pow(10, float64(places))
	return math.Round(value*pow) / pow
}

func normalizeUnit(value float64, unit string) (float64, error) {
	switch unit {
	case "lqi":
		return value * 100 / 255, nil
	case "°F":
		return (value - 32) * 5 / 9, nil
	case "°K":
		return value - 273.15, nil
	case "mV", "mW", "mA":
		return value / 1000, nil
	case "V", "%", "A", "W", "Wh":
		return value, nil
	}
	return 0, fmt.Errorf("Unknown unit %s", unit)
}

// convert Mired to Kelvin and Kelvin to Mired
// Mired = 1,000,000 / Temperature in Kelvin
func convertMired(value int) int {
	return 1e6 / int(value)
}

func convertToFloat(value interface{}) (float64, error) {
	switch v := value.(type) {
	case float32:
		return float64(v), nil
	case float64:
		return v, nil
	case int:
		return float64(v), nil
	case uint:
		return float64(v), nil
	case int8:
		return float64(v), nil
	case uint8:
		return float64(v), nil
	case int16:
		return float64(v), nil
	case uint16:
		return float64(v), nil
	case int32:
		return float64(v), nil
	case uint32:
		return float64(v), nil
	case int64:
		return float64(v), nil
	case uint64:
		return float64(v), nil
	default:
		return 0.0, fmt.Errorf("Unable to converts %v (%T)", value, value)
	}
}

func convertToInt(value interface{}) (int, error) {
	switch v := value.(type) {
	case float32:
		return int(v), nil
	case float64:
		return int(v), nil
	case int:
		return v, nil
	case uint:
		return int(v), nil
	case int8:
		return int(v), nil
	case uint8:
		return int(v), nil
	case int16:
		return int(v), nil
	case uint16:
		return int(v), nil
	case int32:
		return int(v), nil
	case uint32:
		return int(v), nil
	case int64:
		return int(v), nil
	case uint64:
		return int(v), nil
	default:
		return 0, fmt.Errorf("Unable to converts %v (%T)", value, value)
	}
}
